package com.arquitectura;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
public class Libro {
	private String isbn;
	private String titulo;
	private String categoria;
	
	public Libro() {
		this.isbn = "";
		this.titulo = "";
		this.categoria= "";
	}
	public Libro(String isbn, String titulo, String categoria)
	{
		this.categoria = categoria;
		this.titulo = titulo;
		this.isbn = isbn;
	}
	public Libro(String isbn)
	{
		this.isbn=isbn;
		this.titulo="";
		this.categoria="";
			
	}
	public static List<String> buscarTodasLasCategorias() throws Exception, SQLException
	{
		String consultaSQL = "select distinct(Categoria) from Libros";
		DataBaseHelper helper= new DataBaseHelper();
		try
			{List<String> listaCategorias = helper.seleccionarRegistros(consultaSQL,String.class);
			}catch(SQLException e ){
						System.out.println("Error al obtener las categorias" + e.getMessage());
				}catch(Exception e ){
					System.out.println("Error al obtener las categorias" + e.getMessage());
				}
}
		return listaCategorias;
	}
	
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	

	public  void insertar() throws DataBaseException{
		String consultaSQL = "insert into Libros(isbn,Titulo,Categoria) values";
		consultaSQL +="('" + isbn + "','" + titulo + "','" + categoria + "')";
		DataBaseHelper helper = new DataBaseHelper();
		try {
		helper.modificarRegistro(consultaSQL);
		}catch(SQLException e )
		{
			System.out.println("Error de SQL" + e.getMessage());
			throw new DataBaseException("Error en la consulta SQL", e);
		}catch(ClassNotFoundException e ) {
			System.out.println("Error al modificar el registro" + e.getMessage());
			throw new DataBaseException("Error la clase no se encontro", e);
		}
	}
	
	public static  List<Libro> buscarTodos() throw DataBaseException
	{
		
		String consultaSQL = "select isbn, Titulo, Categoria from Libros";
		DataBaseHelper helper= new DataBaseHelper();
		
		{List<Libro> listaDeLibros = helper.seleccionarRegistros(consultaSQL,Libro.class);
		}catch(DataBaseException e ){
			throw 
		}
			
		return listaDeLibros;
	}
	
	public void borrar() throws ClassNotFoundException, 
	SQLException
	{
		String consultaSQL = "delete from libros where isbn="+ this.isbn+"";
		DataBaseHelper<Libro> helper= new DataBaseHelper<Libro>();
		
		try{helper.modificarRegistro(consultaSQL);}catch(SQLException e ){
			System.out.println("Error en consulta borrar libro" + e. getMessage() );
			throw e;
		}catch(ClassNotFoundException e ) {
			System.out.println("Error en borrar libro"+ e.getMessage());
			throw e;
		}
	}
	
	public void salvar() throws SQLException, ClassNotFoundException {
		String consultaSQL = "update libros set titulo='" + this.titulo+
				"', categoria='"+ this.categoria+"' where isbn=" + this.isbn+ "";
		DataBaseHelper<Libro> helper = new DataBaseHelper<Libro>();
		
		try{helper.modificarRegistro(consultaSQL);}catch(SQLException e ) {
			System.out.println("Error en la consulta guardar  registro" + e.getMessage());
			throw e;
		}catch(ClassNotFoundException e ) {
			System.out.println("Error al guardar registro" + e.getMessage());
		}
	}
	
	public static Libro buscarPorClave(String isbn)
	{
		String consultaSQL="select isbn, titulo, categoria from Libros where "+
				"isbn="+ isbn+"";
		DataBaseHelper<Libro> helper= new DataBaseHelper<Libro>();
		List<Libro> listaDeLibros= helper.seleccionarRegistros(consultaSQL,Libro.class);
		return listaDeLibros.get(0);
	}
	
	public static List<Libro> buscarPorCategoria(String categoria)
	{
		String consultaSQL = "select isbn, titulo,categoria from Libros where categoria='"+
				categoria+"'";
		DataBaseHelper<Libro> helper= new DataBaseHelper<Libro>();
		List<Libro> listaDeLibros = helper.seleccionarRegistros(consultaSQL, Libro.class);
		return listaDeLibros;
	}
} 
